package com.company;

public class Car {
    private String uniqueNumber;
    private int timeLeft;
    private boolean isOnPark;

    public String getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public int getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(int timeLeft) {
        timeLeft = timeLeft;
    }

    public void minusOneStep() {
        this.timeLeft = this.timeLeft - 1;
    }

    public Car(String uniqueNumber, int timeLeft, boolean isOnPark) {
        this.uniqueNumber = uniqueNumber;
        this.timeLeft = timeLeft;
        this.isOnPark = isOnPark;
    }

    public boolean isOnPark() {
        return isOnPark;
    }

    public void setOnPark(boolean onPark) {
        isOnPark = onPark;
    }
}
