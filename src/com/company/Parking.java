package com.company;

public class Parking {
    private int numberOfFreePlaces;
    private int numberOfParkingPlaces;
    private int minimalTimeLeft;
    public Car[] carArray;

    public Parking(int freePlaces, Car[] cars) {
        this.numberOfFreePlaces = freePlaces;
        this.carArray = cars;
    }

    public int getNumberOfFreePlaces()
    {
        return numberOfFreePlaces;
    }

    public void setNumberOfFreePlaces(int numberOfFreePlaces)
    {
        this.numberOfFreePlaces = numberOfFreePlaces;
    }

    public int getNumberOfParkingPlaces() {
        return numberOfParkingPlaces;
    }

    public void setNumberOfParkingPlaces(int numberOfParkingPlaces) {
        this.numberOfParkingPlaces = numberOfParkingPlaces;
    }

    public int getMinimalTimeLeft() {
        return minimalTimeLeft;
    }

    public void setMinimalTimeLeft(int minimalTimeLeft) {
        this.minimalTimeLeft = minimalTimeLeft;
    }

    public void setCarArray(Car[] carArray) {
        this.carArray = carArray;
    }

    public void minusOneFreePlace() {
        this.numberOfFreePlaces = this.numberOfFreePlaces - 1;
    }

    public boolean[] clearAllSpace(int parkingSpace, boolean[] isUniCodeFree) {
        this.setNumberOfFreePlaces(parkingSpace);
        for (int i = 0; i < 10000; i++) {
            isUniCodeFree[i] = false;
        }
        for (int i = 0; i < parkingSpace; i++) {
            carArray[i].setTimeLeft(1);
            carArray[i].setOnPark(false);// чтоб в след ходе обнулилось
        }
        return isUniCodeFree;
    }

    public void writeInfo(int parkingSpace) {
        System.out.println("Free Parking places: " + getNumberOfFreePlaces());
        int occupiedPPlace = parkingSpace - getNumberOfFreePlaces();
        System.out.println("Occupied Parking places: " + occupiedPPlace);
        System.out.println("Next place will be free in " + getMinimalTimeLeft() + " step(s)");
        System.out.println();
    }
}
