package com.company;
import java.util.Scanner;

public class ParkingManager {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("How many Parking places will there be");
        int parkingSpace = scan.nextInt();
        Car[] cars = new Car[parkingSpace];
        Parking parking = new Parking(parkingSpace, cars);
        parking.setNumberOfFreePlaces(parkingSpace);
        boolean isEnough = false;
        boolean skippingStepChecker = false;
        boolean[] uniqueCodeChecker = new boolean[10000];
        for (int i = 0; i < 10000; i++) {
            uniqueCodeChecker[i] = false;
        }
        parking.setMinimalTimeLeft(11);
        for (int i = 0; i < parkingSpace; i++) {
            parking.carArray[i] = new Car("0", 1, false); //
        }
        while (!isEnough) {
            ParkingManager.minusTimeLeft(parking.carArray, parkingSpace);
            ParkingManager.carsLeftParking(parking.carArray, parkingSpace);
            if (parking.getNumberOfFreePlaces() != 0) {
                int carCount = (int) (Math.random() * (parkingSpace / 2));
                while (parking.getNumberOfFreePlaces() > 0 & carCount > 0) {
                    int carTime = 1 + (int) (Math.random() * 10);
                    String code = generateUniqueCode(uniqueCodeChecker);
                    int intCode = returnIntegerUniqueCode(code);
                    uniqueCodeChecker[intCode] = true;
                    for (int i = 0; i < parkingSpace; i++) {
                        if (!parking.carArray[i].isOnPark()) {
                            parking.carArray[i].setOnPark(true);
                            parking.carArray[i].setTimeLeft(carTime);
                            parking.carArray[i].setUniqueNumber(code);
                            carCount = carCount - 1;
                            if (parking.getMinimalTimeLeft() > carTime) {
                                parking.setMinimalTimeLeft(carTime);
                            }
                            parking.minusOneFreePlace();
                            break;
                        }
                    }
                    if (parking.getNumberOfFreePlaces() == 0) {
                        printFullParking(parking);
                    }
                }
            } else {
                printFullParking(parking);
            }
            skippingStepChecker = false;
            while (skippingStepChecker == false) {
                System.out.println("choose number 1-3:");
                System.out.println("1.Skip step");
                System.out.println("2.Get info about parking");
                System.out.println("3.Clear parking from all cars");
                int userButton = scan.nextInt();
                switch (userButton) {
                    case 1:
                        skippingStepChecker = true;
                        System.out.println("Step is over");
                        System.out.println();
                        break;
                    case 2:
                        parking.writeInfo(parkingSpace);
                        break;
                    case 3:
                        uniqueCodeChecker = parking.clearAllSpace(parkingSpace, uniqueCodeChecker);
                        System.out.println("The Parking is cleared of cars");
                        break;
                }
            }
        }

    }

    public static Car[] minusTimeLeft(Car[] cars, int parkingSpace) {
        for (int i = 0; i < parkingSpace; i++) {
            int carTime = cars[i].getTimeLeft();
            carTime = carTime - 1;
            cars[i].setTimeLeft(carTime);
        }
        return cars;
    }

    public static Car[] carsLeftParking(Car[] cars, int parkingSpace) {
        for (int i = 0; i < parkingSpace; i++) {
            if (cars[i].getTimeLeft() == 0) {
                cars[i].setOnPark(false);
                cars[i].setUniqueNumber("0");
            }
        }
        return cars;
    }

    public static String generateUniqueCode(boolean[] isUniqueCodBusy) {
        boolean isFind = false;
        while (!isFind) {
            int digit1 = (int) (Math.random() * 10);
            int digit2 = (int) (Math.random() * 10);
            int digit3 = (int) (Math.random() * 10);
            int digit4 = (int) (Math.random() * 10);
            int uniqueCode = digit4 + digit3 * 10 + digit2 * 100 + digit1 * 1000;
            char char1 = (char) digit1;
            String uniqueCodeString = "" + digit1 + digit2 + digit3 + digit4;
            if (!isUniqueCodBusy[uniqueCode]) {
                isFind = true;
                return uniqueCodeString;
            }
        }
        return "0";
    }

    public static void printFullParking(Parking parking) {
        System.out.println("Parking is full");
        System.out.println("Next place will free in" + parking.getMinimalTimeLeft() + " step(s)");
        System.out.println();
    }

    public static int returnIntegerUniqueCode(String uniqueCode) {
        char[] arr = uniqueCode.toCharArray();
        int digit1 = arr[0] - '0';
        int digit2 = arr[1] - '0';
        int digit3 = arr[2] - '0';
        int digit4 = arr[3] - '0';

        int uniqueCodeInteger = digit4 + digit3 * 10 + digit2 * 100 + digit1 * 1000;

        return uniqueCodeInteger;
    }
}


